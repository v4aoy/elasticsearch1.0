//--------------------------------------------------------------
// Description : pager-taglib标签扩展
// Creater : 张皓
// Create Date : 2010-11-20 10:10:12 
//--------------------------------------------------------------
// pager-taglib.js

// 请求提交
function doPostBack(pagerName, arg) {
	try{
		var offsetInput = document.getElementsByName(pagerName + '.offset')[0];
		if (isNaN(arg)) {
			// 每页记录数改变
			if (arg.toLowerCase() == 'maxpageitems') {
				PT_CheckSubmit(pagerName,1);
			} 
			// 页面跳转
			else if (arg.toLowerCase() == 'pagenumber') {
				PT_CheckSubmit(pagerName,2);
			}
		}
		// 如arg为数字表示跳转至某页
		else {
			if (arg > 0)
				arg--; // 索引从0开始，此处减1
			offsetInput.value = arg;
			PT_CheckSubmit(pagerName,1);
		}
	} catch(e){
		alert('分页出现异常:'+e.message);
	}
}

// 提交验证
function PT_CheckSubmit(pagerName, type){
	var submit=document.getElementsByName(pagerName + '.submit')[0];
	
	// 直接提交
	if(type==1){
		var clickObj=submit.onclick;
		submit.onclick=null;	// 去除onclick事件，以免进行"转到"操作
		submit.click();
		submit.onclick=clickObj;// 恢复onclick事件
	}
	// 验证跳转输入信息
	else if(type==2){
		var offsetInput = document.getElementsByName(pagerName + '.offset')[0];
		var pageInput = document.getElementsByName(pagerName + '.pageNumber')[0];
		var pageSizeInput = document.getElementsByName(pagerName + '.maxPageItems')[0];
		var oldVal=pageInput.getAttribute("number");
		var skipNumber=pageInput.value;
		var pages=pageInput.getAttribute("pages");
		while(skipNumber.indexOf(' ')>0) skipNumber=skipNumber.replace(' ','');
		if(skipNumber=='') return false;
		// 非数字则恢复原值
		if(isNaN(skipNumber)) {
			pageInput.value=oldVal;
			pageInput.focus();
			return false;
		}
		if(parseInt(skipNumber)<=0||parseInt(skipNumber)>pages){
			alert('索引超出范围！');
			pageInput.focus();
			return false;
		}
		// 计算记录索引
		offsetInput.value = (skipNumber-1) * pageSizeInput.value;
	}
}
