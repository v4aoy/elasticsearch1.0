package com.elasticsearch.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 功能:编码过滤器类 作者:jackKang 时间:2011-3-12 下午09:50:35 版本:1.0
 */
public class EncodingFilter implements Filter {
	protected FilterConfig filterConfig;
	private String targetEncoding = "UTF-8";

	/**
	 * 初始化过滤器,和一般的Servlet一样，它也可以获得初始参数。
	 */
	public void init(FilterConfig config) throws ServletException {
		this.filterConfig = config;
		this.targetEncoding = config.getInitParameter("encoding");
	}

	/**
	 * 进行过滤处理，这个方法最重要，所有过滤处理的代码都在此实现。
	 */
	public void doFilter(ServletRequest srequest, ServletResponse sresponse,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) srequest;
		HttpServletResponse response = (HttpServletResponse) sresponse;
		request.setCharacterEncoding(targetEncoding);
		response.setCharacterEncoding(targetEncoding);
		// 把处理权发送到下一个
		chain.doFilter(request, response);
	}

	public void setFilterConfig(final FilterConfig filterConfig) {
		this.filterConfig = filterConfig;
	}

	// 销毁过滤器
	public void destroy() {
		this.filterConfig = null;
	}
}
