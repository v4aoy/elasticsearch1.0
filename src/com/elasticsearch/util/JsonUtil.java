package com.elasticsearch.util;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * 功能:将数据格式转换成json工具类 作者:jackKang 时间:2011-03-12 21:09:10 版本:1.0
 */
public class JsonUtil {

	/**
	 * 
	 * 功能：实现将一个ojb对象转换成json格式数据的方法 时间:2011-3-12 下午09:24:23
	 * 
	 * @param obj
	 * @return:返回封装好的json格式数据
	 */
	@SuppressWarnings("unchecked")
	public static String object2json(Object obj) {
		StringBuilder json = new StringBuilder();
		if (obj == null) {
			json.append("\"\"");
		} else if (obj instanceof String || obj instanceof Integer
				|| obj instanceof Float || obj instanceof Boolean
				|| obj instanceof Short || obj instanceof Double
				|| obj instanceof Long || obj instanceof BigDecimal
				|| obj instanceof BigInteger || obj instanceof Byte) {
			json.append("\"").append(string2json(obj.toString())).append("\"");
		} else if (obj instanceof Object[]) {
			json.append(array2json((Object[]) obj));
		} else if (obj instanceof List) {
			json.append(list2json((List<?>) obj));
		} else if (obj instanceof Map) {
			json.append(map2json((Map<?, ?>) obj));
		} else if (obj instanceof Set) {
			json.append(set2json((Set<?>) obj));
		} else {
			json.append(bean2json(obj));
		}
		return json.toString();
	}

	/**
	 * 
	 * 功能：将一个实体对象封装成json格式数据的方法 时间:2011-3-12 下午09:25:44
	 * 
	 * @param bean
	 * @return:json格式Bean对象
	 */
	public static String bean2json(Object bean) {
		StringBuilder json = new StringBuilder();
		json.append("{");
		PropertyDescriptor[] props = null;
		try {
			props = Introspector.getBeanInfo(bean.getClass(), Object.class)
					.getPropertyDescriptors();
		} catch (IntrospectionException e) {
		}
		if (props != null) {
			for (int i = 0; i < props.length; i++) {
				try {
					String name = object2json(props[i].getName());
					String value = object2json(props[i].getReadMethod().invoke(
							bean));
					json.append(name);
					json.append(":");
					json.append(value);
					json.append(",");
				} catch (Exception e) {
					System.out.println("转换出错:" + e.getMessage());
				}
			}
			json.setCharAt(json.length() - 1, '}');
		} else {
			json.append("}");
		}
		/* 返回转换好的json格式 */
		return json.toString();
	}

	/**
	 * 
	 * 功能：将一个List集合对象转换成json格式的方法 时间:2011-3-12 下午09:26:51
	 * 
	 * @param list
	 *            :集合对象
	 * @return:转换后的json List对象
	 */
	public static String list2json(List<?> list) {
		StringBuilder json = new StringBuilder();
		json.append("[");
		/* 非空判断集合对象 */
		if (list != null && list.size() > 0) {
			for (Object obj : list) {
				json.append(object2json(obj));
				json.append(",");
			}
			json.setCharAt(json.length() - 1, ']');
		} else {
			json.append("]");
		}
		/* 返回数据转换后的集合json对象 */
		return json.toString();
	}

	/**
	 * 功能：将一个数组转换成json格式方法 时间:2011-3-12 下午09:28:24
	 * 
	 * @param array
	 *            :数组
	 * @return:转换后的json对象格式数据
	 */
	public static String array2json(Object[] array) {
		StringBuilder json = new StringBuilder();
		json.append("[");
		if (array != null && array.length > 0) {
			for (Object obj : array) {
				json.append(object2json(obj));
				json.append(",");
			}
			json.setCharAt(json.length() - 1, ']');
		} else {
			json.append("]");
		}
		return json.toString();
	}

	/**
	 * 
	 * 功能：将一个Map集合转换成json格式数据方法 时间:2011-3-12 下午09:29:35
	 * 
	 * @param map
	 * @return
	 */
	public static String map2json(Map<?, ?> map) {
		StringBuilder json = new StringBuilder();
		json.append("{");
		/* 非空判断 */
		if (map != null && map.size() > 0) {
			for (Object key : map.keySet()) {
				json.append(object2json(key));
				json.append(":");
				json.append(object2json(map.get(key)));
				json.append(",");
			}
			json.setCharAt(json.length() - 1, '}');
		} else {
			json.append("}");
		}
		/* 返回转换后的json数据格式 */
		return json.toString();
	}

	/**
	 * 功能：将set集合转换成json格式数据的方法 时间:2011-3-12 下午09:30:24
	 * 
	 * @param set
	 *            :Set对象
	 * @return:转换后的json格式数据
	 */
	public static String set2json(Set<?> set) {
		StringBuilder json = new StringBuilder();
		json.append("[");
		if (set != null && set.size() > 0) {
			for (Object obj : set) {
				json.append(object2json(obj));
				json.append(",");
			}
			json.setCharAt(json.length() - 1, ']');
		} else {
			json.append("]");
		}
		/* 返回转换拼接后的json格式 */
		return json.toString();
	}

	/**
	 * 功能：将一个String格式的数据转换成json格式的方法 时间:2011-3-12 下午09:31:41
	 * 
	 * @param s
	 *            :参数
	 * @return:转换后的json格式
	 */
	public static String string2json(String s) {
		if (s == null)
			return "";
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			switch (ch) {
			case '"':
				sb.append("\\\"");
				break;
			case '\\':
				sb.append("\\\\");
				break;
			case '\b':
				sb.append("\\b");
				break;
			case '\f':
				sb.append("\\f");
				break;
			case '\n':
				sb.append("\\n");
				break;
			case '\r':
				sb.append("\\r");
				break;
			case '\t':
				sb.append("\\t");
				break;
//			case '/':
//				sb.append("\\/");
//				break;
			default:
				if (ch >= '\u0000' && ch <= '\u001F') {
					String ss = Integer.toHexString(ch);
					sb.append("\\u");
					for (int k = 0; k < 4 - ss.length(); k++) {
						sb.append('0');
					}
					sb.append(ss.toUpperCase());
				} else {
					sb.append(ch);
				}
			}
		}
		return sb.toString();
	}

}
