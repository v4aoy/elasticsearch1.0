package com.elasticsearch.config;

import java.io.File;
import java.util.Properties;

public class Config {
	private final static String PROPERTIES_FILE = "elasticsearch.properties";
	private static Properties properties;

	/**
	 * 加载属性文件
	 * 
	 * @param aDirPath
	 */
//	public static void load(String aDirPath) {
//
//		try {
//			properties = PropertyUtil.getProperties(PROPERTIES_FILE);
//		} catch (Throwable t) {
//			String filePath = aDirPath + File.separator + PROPERTIES_FILE;
//			try {
//				properties = PropertyUtil.getProperties(filePath);
//			} catch (Throwable t2) {
//				return;
//			}
//		}
//	}
	
	
	  static {   
		  String filePath =File.separator + PROPERTIES_FILE;
			try {
				properties = PropertyUtil.getProperties(filePath);
			} catch (Throwable t2) {
				
			}  
	    } 

	/**
	 * 获取属性值返回类型String
	 * 
	 * @param aName
	 * @param aDefault
	 * @return
	 */
	public static String getProperty(String aName, String aDefault) {
		return properties.getProperty(aName, aDefault);
	}

	/**
	 * 获取属性值返回类型boolean
	 * 
	 * @param aName
	 * @param aDefault
	 * @return
	 */
	public static boolean getBoolProperty(String aName, String aDefault) {
		return Boolean.parseBoolean(getProperty(aName, aDefault));
	}

	/**
	 * 获取属性值返回类型int
	 * 
	 * @param aName
	 * @param aDefault
	 * @return
	 */
	public static Integer getIntProperty(String aName, String aDefault) {
		return Integer.parseInt(getProperty(aName, aDefault));
	}

	/**
	 * 获取属性值返回类型long
	 * 
	 * @param aName
	 * @param aDefault
	 * @return
	 */
	public static Long getLongProperty(String aName, String aDefault) {
		return Long.parseLong(getProperty(aName, aDefault));
	}
}
